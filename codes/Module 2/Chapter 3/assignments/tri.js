function check() {
    let t11 = document.getElementById("t11");
    let t12 = document.getElementById("t12");
    let t21 = document.getElementById("t21");
    let t22 = document.getElementById("t22");
    let t31 = document.getElementById("t31");
    let t32 = document.getElementById("t32");
    let t1 = document.getElementById("t1");
    let t2 = document.getElementById("t2");
    let ans = document.getElementById("ans");
    var x1 = parseFloat(t11.value);
    var y1 = parseFloat(t12.value);
    var x2 = parseFloat(t21.value);
    var y2 = parseFloat(t22.value);
    var x3 = parseFloat(t31.value);
    var y3 = parseFloat(t32.value);
    var x = parseFloat(t1.value);
    var y = parseFloat(t2.value);
    var a = Math.abs((x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) / 2); // Area of the Main Triangle..
    //Areas of the smaller triangles formed..
    var b = Math.abs((x * (y1 - y2) + x1 * (y2 - y) + x2 * (y - y1)) / 2);
    var c = Math.abs((x * (y2 - y3) + x2 * (y3 - y) + x3 * (y - y2)) / 2);
    var d = Math.abs((x * (y3 - y1) + x3 * (y1 - y) + x1 * (y - y3)) / 2);
    //Sum of area of Smaller triangles formed.. 
    var s = b + c + d;
    var e = Math.abs(s - a);
    if (e < 0.0000001) // Condition to check if point lies inside the triangle..
     {
        document.getElementById("ans").innerHTML = "The point does lies in the triangle.";
    }
    else {
        document.getElementById("ans").innerHTML = "The point does not lies in the triangle.";
    }
}
//# sourceMappingURL=tri.js.map